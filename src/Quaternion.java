import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double r;                    // Real part
   private double i;                    // Imaginary part i
   private double j;                    // Imaginary part j
   private double k;                    // Imaginary part k
   private static double precision = 0.000001; // Precision for close to zero comparisons

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      r = a;
      i = b;
      j = c;
      k = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() { return r; }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() { return i; }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() { return j; }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() { return k; }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String operatorI = "+";
      String operatorJ = "+";
      String operatorK = "+";

      if (getIpart() < 0) {
         operatorI = "";
      }
      if (getJpart() < 0) {
         operatorJ = "";
      }
      if (getKpart() < 0) {
         operatorK = "";
      }
      return
          this.getRpart() + operatorI +
          this.getIpart() + "i" + operatorJ +
          this.getJpart() + "j"  + operatorK +
          this.getKpart() + "k";
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      double r;
      double i;
      double j;
      double k;
      try {
         String[] items = s.trim().split("(?=[+-]\\d+)");
         if (s == null || items.length == 0) {
            throw new RuntimeException("Your input is null or empty: " + s);
         }
         if (items.length > 4) {
           throw new RuntimeException("You have too many items: " + items.length + " in your input: " + s);
         } else {                           // Handle inputs that have 1-4 items
           int countI = 0;
           int countJ = 0;
           int countK = 0;
           int countR = 0;
           String rStringValue = "";
           String iStringValue = "";
           String jStringValue = "";
           String kStringValue = "";
           for (String item : items) {
               String[] chars = item.trim().split("(?=[ijk])");
               if (chars.length > 1) {
                 if (chars[1].equals("i") && chars.length < 3) {
                   iStringValue = chars[0];
                   countI++;
                 } else if (chars[1].equals("j") && chars.length < 3) {
                   jStringValue = chars[0];
                   countJ++;
                 } else if (chars[1].equals("k") && chars.length < 3) {
                   kStringValue = chars[0];
                   countK++;
                 } else {throw new RuntimeException("Your input: " + s + " contains illegal characters");}
               } else {
                 rStringValue = item;
                 countR++;
               }
             }
             if (countR > 1 || countI > 1 || countJ > 1 || countK > 1) {
               throw new RuntimeException("Your input: " + s + " contains double values or illegal characters");
             } else {
               if (countR == 1) {
                 r = Double.parseDouble(rStringValue.replace("+", ""));
               } else {
                 r = 0.0;
               }
               if (countI == 1) {
                 i = Double.parseDouble(iStringValue.replace("i", "").replace("+", ""));
               } else {
                 i = 0.0;
               }
               if (countJ == 1) {
                 j = Double.parseDouble(jStringValue.replace("j", "").replace("+", ""));
               } else {
                 j = 0.0;
               }
               if (countK == 1) {
                 k = Double.parseDouble(kStringValue.replace("k", "").replace("+", ""));
               } else {
                 k = 0.0;
               }
             }
         }
      } catch (IllegalArgumentException iae) {
         throw new IllegalArgumentException("There was something wrong with your input: " + s + ". Can't convert it to Quaternion");
      }
      Quaternion toQuaternion = new Quaternion(r, i, j, k);
      return toQuaternion;
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(r, i, j, k);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return equals(new Quaternion(0, 0, 0, 0));
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(r, i*-1, j*-1, k*-1);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(r*-1, i*-1, j*-1, k*-1);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
     return new Quaternion (r+q.r, i+q.i, j+q.j, k+q.k);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      Quaternion timesQ = new Quaternion(r*q.r - i*q.i - j*q.j - k*q.k,
                                         r*q.i + i*q.r + j*q.k - k*q.j,
                                         r*q.j - i*q.k + j*q.r + k*q.i,
                                         r*q.k + i*q.j - j*q.i + k*q.r
                                         );
      return timesQ;
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.r * r, i * r, j * r, k * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("You can't divide with zero");
      }
      Quaternion inverseQ = new Quaternion(r / (r*r + i*i + j*j + k*k),
                                          i*-1 / (r*r + i*i + j*j + k*k),
                                          j*-1 / (r*r + i*i + j*j + k*k),
                                          k*-1 / (r*r + i*i + j*j + k*k)
                                          );
      return inverseQ;
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion (r-q.r, i-q.i, j-q.j, k-q.k);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("You can't divide with zero R");
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("You can't divide with zero");
      }
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    *
    * LOGIC FOR COMPARING CLOSE TO ZERO NUMBERS:
    * https://stackoverflow.com/questions/1088216/whats-wrong-with-using-to-compare-floats-in-java/1088271#1088271
    */
   @Override
   public boolean equals (Object qo) {
     double compR = ((Quaternion) qo).r;
     double compI = ((Quaternion) qo).i;
     double compJ = ((Quaternion) qo).j;
     double compK = ((Quaternion) qo).k;

     return
         Math.abs(this.getRpart() - compR) < precision &&
         Math.abs(this.getIpart() - compI) < precision &&
         Math.abs(this.getJpart() - compJ) < precision &&
         Math.abs(this.getKpart() - compK) < precision;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion p = this;
      Quaternion tmpDm = p.times(q.conjugate()).plus(q.times(p.conjugate()));

      tmpDm.r = tmpDm.r / 2;
      tmpDm.i = tmpDm.i / 2;
      tmpDm.j = tmpDm.j / 2;
      tmpDm.k = tmpDm.k / 2;
      return tmpDm;
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(r, i, j, k);
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(r*r+i*i+j*j+k*k) ;
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
     /*System.out.println("-1-2i-3j-4k: " + valueOf("-1-2i-3j-4k"));
     System.out.println("-1: " + valueOf("-1"));
     System.out.println("-1+3j: " + valueOf("-1+3j"));
     System.out.println("3k-4j: " + valueOf("3k-4j"));
     System.out.println("1+2i+3k+4j: " + valueOf("1+2i+3k+4j"));
     //System.out.println("1+2i+3i+4k: " + valueOf("1+2i+3i+4k"));
     //System.out.println("1+2i+3j+4k+5k: " + valueOf("1+2i+3j+4k+5k"));
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));*/
      System.out.println (valueOf("-1-2i-3j-4k-"));
   }
}
// end of file
